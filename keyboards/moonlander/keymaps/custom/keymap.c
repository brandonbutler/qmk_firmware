#include QMK_KEYBOARD_H
#include "version.h"
#include "keymap_german.h"
#include "keymap_nordic.h"
#include "keymap_french.h"
#include "keymap_spanish.h"
#include "keymap_hungarian.h"
#include "keymap_swedish.h"
#include "keymap_br_abnt2.h"
#include "keymap_canadian_multilingual.h"
#include "keymap_german_ch.h"
#include "keymap_jp.h"
#include "keymap_korean.h"
#include "keymap_bepo.h"
#include "keymap_italian.h"
#include "keymap_slovenian.h"
#include "keymap_lithuanian_azerty.h"
#include "keymap_danish.h"
#include "keymap_norwegian.h"
#include "keymap_portuguese.h"
#include "keymap_contributions.h"
#include "keymap_czech.h"
#include "keymap_romanian.h"
#include "keymap_russian.h"
#include "keymap_uk.h"
#include "keymap_estonian.h"
#include "keymap_belgian.h"
#include "keymap_us_international.h"

#define KC_MAC_UNDO LGUI(KC_Z)
#define KC_MAC_CUT LGUI(KC_X)
#define KC_MAC_COPY LGUI(KC_C)
#define KC_MAC_PASTE LGUI(KC_V)
#define KC_PC_UNDO LCTL(KC_Z)
#define KC_PC_CUT LCTL(KC_X)
#define KC_PC_COPY LCTL(KC_C)
#define KC_PC_PASTE LCTL(KC_V)
#define ES_LESS_MAC KC_GRAVE
#define ES_GRTR_MAC LSFT(KC_GRAVE)
#define ES_BSLS_MAC ALGR(KC_6)
#define NO_PIPE_ALT KC_GRAVE
#define NO_BSLS_ALT KC_EQUAL
#define LSA_T(kc) MT(MOD_LSFT | MOD_LALT, kc)
#define BP_NDSH_MAC ALGR(KC_8)
#define MOON_LED_LEVEL LED_LEVEL

enum custom_keycodes {
  RGB_SLD = ML_SAFE_RANGE,
  ST_MACRO_0,
  ST_MACRO_1,
};


/*
  The keymap layout is meant to be identical to the physical layout of the moonlander keyboard.

  To create a new macro, create a new name in `enum custom_keycodes` above. Add that name somewhere in the map below.
  Then add a `switch-case block below in the `process_record_user` function towards the bottom of this file.
  See https://beta.docs.qmk.fm/using-qmk for options
*/
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  /* Base Keyboard layer */
  [0] = LAYOUT_moonlander(
    KC_ESCAPE,      KC_1,           KC_2,           KC_3,           KC_4,           KC_5,           KC_F5,                                          KC_INSERT,      KC_6,           KC_7,           KC_8,           KC_9,           KC_0,           KC_BSPACE,
    KC_TAB,         KC_Q,           KC_W,           KC_E,           KC_R,           KC_T,           KC_MINUS,                                       KC_EQUAL,       KC_Y,           KC_U,           KC_I,           KC_O,           KC_P,           KC_DELETE,
    KC_CAPSLOCK,    KC_A,           KC_S,           KC_D,           KC_F,           KC_G,           TT(2),                                          TT(2),          KC_H,           KC_J,           KC_K,           KC_L,           KC_SCOLON,      KC_ENTER,
    KC_LSHIFT,      KC_Z,           KC_X,           KC_C,           KC_V,           KC_B,                                                                           KC_N,           KC_M,           KC_COMMA,       KC_DOT,         KC_SLASH,       KC_UP,
    MO(1),          KC_LCTRL,       KC_LGUI,        KC_LALT,        MO(3),                                    LCTL(LSFT(KC_S)),        LGUI(KC_TAB),                                KC_LBRACKET,    KC_RBRACKET,    KC_LEFT,        KC_DOWN,        KC_RIGHT,
                                                                                           KC_SPACE, KC_LALT, KC_LGUI,                 TO(2), TO(4), LCTL(LGUI(KC_RIGHT))
  ),

  /* Special character layer */
  [1] = LAYOUT_moonlander(
    KC_GRAVE,       KC_F1,          KC_F2,          KC_F3,          KC_F4,          KC_F5,          KC_F6,                                          KC_F7,          KC_F8,          KC_F9,          KC_F10,         KC_F11,         KC_F12,         KC_TRANSPARENT,
    KC_TRANSPARENT, KC_EXLM,        KC_AT,          KC_LCBR,        KC_RCBR,        KC_GRAVE,       KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_UP,          KC_7,           KC_8,           KC_9,           KC_ASTR,        KC_TRANSPARENT,
    KC_TRANSPARENT, KC_HASH,        KC_DLR,         KC_LPRN,        KC_RPRN,        KC_DQUO,        TT(2),                                          KC_TRANSPARENT, KC_DOWN,        KC_4,           KC_5,           KC_6,           KC_KP_PLUS,     KC_KP_MINUS,
    KC_TRANSPARENT, LCTL(KC_Z),     KC_PIPE,        KC_LBRACKET,    KC_RBRACKET,    KC_QUOTE,                                                                       KC_AMPR,        KC_1,           KC_2,           KC_3,           KC_BSLASH,      KC_SLASH,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_LABK,        KC_RABK,        KC_TRANSPARENT,                          LGUI(KC_SPACE),           LCTL(KC_UP),                                 KC_TRANSPARENT, KC_DOT,         KC_0,           KC_EQUAL,       KC_KP_ENTER,
                                                                                       KC_F9, LCTL(KC_LGUI), TT(2),                    LGUI(LSFT(KC_ESCAPE)), LCTL(KC_LEFT), LCTL(KC_RIGHT)
  ),

  /* Gaming layer */
  [2] = LAYOUT_moonlander(
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_6,                                           KC_7,           KC_8,           KC_9,           KC_0,           KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_BSLASH,      KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    MO(3),          KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_SLASH,                                  KC_AMPR,                KC_TRANSPARENT,                              KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
                                                                                               KC_SPACE, KC_X, KC_CAPSLOCK,            TO(0), KC_TRANSPARENT, KC_TRANSPARENT
  ),

  /* Misc. Shortcuts layer */
  [3] = LAYOUT_moonlander(
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, DYN_REC_START1, DYN_REC_STOP,   DYN_MACRO_PLAY1,KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, LSFT(KC_F2),    LALT(KC_F10),                                                                   KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                        KC_TRANSPARENT,             KC_TRANSPARENT,                              KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
                                                                                KC_MAC_COPY, KC_MAC_PASTE, KC_TRANSPARENT,             KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT
  ),

  /* World of Warcraft layer */
  [4] = LAYOUT_moonlander(
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, ST_MACRO_0,     KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                          KC_TRANSPARENT,           KC_TRANSPARENT,                              KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
                                                                             KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,           KC_TRANSPARENT, TO(0), KC_TRANSPARENT
  ),
};

extern bool g_suspend_state;
extern rgb_config_t rgb_matrix_config;

void keyboard_post_init_user(void) {
  rgb_matrix_enable();
}

/*
  LEDs have a different map layout than keys.
  The order starts on the top-left, and reads downward, shifting to the right once for each line.
  The second keyboard starts on the top-right, and reads downward, shifting to the left once for each line.

  The lines with an out of place sequence of 4 elements is for the thumb buttons.
  The first 3 elements are for the small thumb buttons (following the same direction as the columns). The last is for the larger thumb button.
*/
const uint8_t PROGMEM ledmap[][DRIVER_LED_TOTAL][3] = {
    /* Base Keyboard layer */
    [0] = { {249,228,255}, {205,255,255}, {205,255,255}, {205,255,255}, {12,225,241},
            {141,255,233}, {146,224,255}, {146,224,255}, {146,224,255}, {205,255,255},
            {141,255,233}, {146,224,255}, {146,224,255}, {146,224,255}, {205,255,255},
            {141,255,233}, {146,224,255}, {146,224,255}, {146,224,255}, {205,255,255},
            {141,255,233}, {141,255,233}, {146,224,255}, {146,224,255}, {12,225,241},
            {141,255,233}, {146,224,255}, {146,224,255}, {146,224,255},
            {205,255,255}, {146,224,255}, {12,225,241},        {205,255,255}, {105,255,255}, {105,255,255}, {205,255,255},

            {249,228,255}, {249,228,255}, {205,255,255}, {14,222,242},  {14,222,242},
            {146,224,255}, {141,255,233}, {146,224,255}, {146,224,255}, {14,222,242},
            {146,224,255}, {141,255,233}, {146,224,255}, {146,224,255}, {14,222,242},
            {146,224,255}, {146,224,255}, {146,224,255}, {141,255,233}, {146,224,255},
            {146,224,255}, {146,224,255}, {146,224,255}, {146,224,255}, {146,224,255},
            {146,224,255}, {146,224,255}, {146,224,255}, {146,224,255},
            {205,255,255}, {146,224,255}, {12,225,241},        {105,255,255}, {134,255,213}, {0,204,255}, {205,255,255}
         },

    /* Special character layer */
    [1] = { {12,225,241},  {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {205,255,255}, {12,225,241},  {12,225,241},  {12,225,241},  {0,0,0},
            {205,255,255}, {12,225,241},  {12,225,241},  {12,225,241},  {12,225,241},
            {205,255,255}, {12,225,241},  {12,225,241},  {12,225,241},  {12,225,241},
            {205,255,255}, {12,225,241},  {12,225,241},  {12,225,241},  {12,225,241},
            {205,255,255}, {12,225,241},  {12,225,241},  {12,225,241},
            {205,255,255}, {0,0,0},       {0,0,0},             {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0},

            {0,183,238},   {0,183,238},   {12,225,241},  {12,225,241},  {12,225,241},
            {205,255,255}, {12,225,241},  {12,225,241},  {12,225,241},  {12,225,241},
            {205,255,255}, {12,225,241},  {12,225,241},  {12,225,241},  {12,225,241},
            {205,255,255}, {12,225,241},  {12,225,241},  {12,225,241},  {12,225,241},
            {205,255,255}, {12,225,241},  {12,225,241},  {12,225,241},  {0,0,0},
            {205,255,255}, {12,225,241},  {12,225,241},  {12,225,241},
            {205,255,255}, {0,0,0},       {0,0,0},            {0,0,0},  {0,0,0}, {0,0,0}, {0,0,0}
         },

    /* Gaming layer */
    [2] = { {0,0,0},       {205,255,255}, {105,255,255}, {205,255,255}, {31,255,255},
            {105,255,255}, {0,0,0},       {249,228,255}, {0,0,0},       {205,255,255},
            {105,255,255}, {249,228,255}, {249,228,255}, {0,0,0},       {205,255,255},
            {105,255,255}, {0,0,0},       {249,228,255}, {0,0,0},       {205,255,255},
            {105,255,255}, {0,0,0},       {0,0,0},       {0,0,0},       {105,255,255},
            {105,255,255}, {0,0,0},       {0,0,0},       {0,0,0},
            {105,255,255}, {0,0,0},       {0,0,0},            {0,0,0},  {0,0,0}, {0,0,0}, {0,0,0},

            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {105,255,255}, {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {105,255,255}, {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {105,255,255}, {0,0,0},       {0,0,0},       {0,0,0},
            {105,255,255}, {0,0,0}, {0,0,0},                  {0,0,0}, {0,0,0}, {31,255,255}, {0,0,0}
         },

    /* Misc. Shortcuts layer */
    [3] = { {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,183,238},   {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,183,238},
            {0,0,0},       {0,0,0},       {0,0,0},            {134,255,213}, {134,255,213}, {0,0,0}, {0,0,0},

            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},            {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0}
         },

    /* World of Warcraft layer */
    [4] = { {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,183,238},   {134,255,213}, {205,255,255}, {31,255,255},  {0,0,0},
            {0,183,238},   {134,255,213}, {205,255,255}, {31,255,255},  {0,0,0},
            {0,183,238},   {134,255,213}, {205,255,255}, {31,255,255},  {0,0,0},
            {0,183,238},   {105,255,255}, {205,255,255}, {31,255,255},  {0,0,0},
            {0,183,238},   {105,255,255}, {205,255,255}, {31,255,255},
            {0,0,0},       {0,0,0},       {0,0,0},            {0,0,0}, {0,0,0}, {0,0,0}, {0,0,0},

            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},       {0,0,0},
            {0,0,0},       {0,0,0},       {0,0,0},            {0,0,0}, {134,255,213}, {0,0,0}, {0,0,0}
         },
};

void set_layer_color(int layer) {
  for (int i = 0; i < DRIVER_LED_TOTAL; i++) {
    HSV hsv = {
      .h = pgm_read_byte(&ledmap[layer][i][0]),
      .s = pgm_read_byte(&ledmap[layer][i][1]),
      .v = pgm_read_byte(&ledmap[layer][i][2]),
    };
    if (!hsv.h && !hsv.s && !hsv.v) {
        rgb_matrix_set_color( i, 0, 0, 0 );
    } else {
        RGB rgb = hsv_to_rgb( hsv );
        float f = (float)rgb_matrix_config.hsv.v / UINT8_MAX;
        rgb_matrix_set_color( i, f * rgb.r, f * rgb.g, f * rgb.b );
    }
  }
}

void rgb_matrix_indicators_user(void) {
  if (g_suspend_state || keyboard_config.disable_layer_led) { return; }
  switch (biton32(layer_state)) {
    case 0:
      set_layer_color(0);
      break;
    case 1:
      set_layer_color(1);
      break;
    case 2:
      set_layer_color(2);
      break;
    case 3:
      set_layer_color(3);
      break;
    case 4:
      set_layer_color(4);
      break;
   default:
    if (rgb_matrix_get_flags() == LED_FLAG_NONE)
      rgb_matrix_set_color_all(0, 0, 0);
    break;
  }
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case ST_MACRO_0:
        if (record->event.pressed) {
            SEND_STRING(SS_TAP(X_5) SS_DELAY(1515) SS_TAP(X_5) SS_DELAY(1637) SS_TAP(X_5) SS_DELAY(1578) SS_TAP(X_5));
        }
        break;
    case ST_MACRO_1:
        if (record->event.pressed) {
            SEND_STRING(SS_TAP(X_A) SS_DELAY(1523) SS_TAP(X_A));
        }
        break;
    case RGB_SLD:
        if (record->event.pressed) {
            rgblight_mode(1);
        }
        return false;
    }
    return true;
}
