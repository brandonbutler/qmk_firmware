# Brandon's fork of ZSA's fork of QMK Firmware 

## About

### Supported Keyboards

* [Moonlander Mark I](/keyboards/moonlander)

### Building

All firmware is built in CI/CD. See the `.gitlab-ci.yml` for instructions on how to build.

## In General

The bread and butter is in `keyboards/moonlander/keymaps/custom` which contains the files necessary for generating my Moonlander's firmware. The most important file in that directory is `keymap.c` which contains two really important Maps.

### Key Map

The first map is located under the constant `keymaps`, which is a map of what keys on the board should do what on a button press. It contains that data for every layer on that board. The keymap layout is meant to be identical to the physical layout of the moonlander keyboard.

#### Macros

To create a new macro, create a new name in `enum custom_keycodes` in the `keymap.c` file. Add that name somewhere in the keymap (wherever you want it to go). 
Then add a switch-case block below in the `process_record_user` function towards the bottom of the same file. 
See https://beta.docs.qmk.fm/using-qmk for options

### LED Map
The second map is located under the constant `ledmap`, which is a map of the colors desginated for each key on the board, on each layer. The ledmap has a different layout than the keymap. The order starts (for the left-side keypad) on the top-left, and reads downward, shifting to the right once for each line. The second (right) keypad starts on the top-right, and reads downward, shifting to the left once for each line.

The lines with an out of place sequence of 4 elements is for the thumb buttons. The first 3 elements are for the small thumb buttons (following the same direction as the columns). The last is for the larger thumb button.
